<?php namespace HeidiMarasigan\Nltc\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use HeidiMarasigan\Nltc\Models\Student;
use HeidiMarasigan\Nltc\Models\Subject;
use HeidiMarasigan\Nltc\Models\Enrollment;
use Db;
use Log;

/**
 * Enrollments Back-end Controller
 */
class Enrollments extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('HeidiMarasigan.Nltc', 'NLTC', 'enrollments');
    }

    public function getEnrollmentStatus()
    {
        if(Enrollment::where('user_id',$this->user->id)->first())
        {
            return true;
        }
        return false;
    }

    public function getSubjects()
    {
        $student  = Student::where('user_id',$this->user->id)->first();
        return Subject::where('level_id',$student->level_id)->get();
    }

    public function getEnrolledSubjects()
    {
        $enrollment  = Enrollment::where('user_id',$this->user->id)->first();
        
        $student  = Student::where('user_id',$this->user->id)->first();

        $subjects_list = Db::table('heidimarasigan_nltc_class_records')
            ->leftJoin('heidimarasigan_nltc_subjects','heidimarasigan_nltc_subjects.id','=','heidimarasigan_nltc_class_records.subject_id')
            ->leftJoin('heidimarasigan_nltc_class_records_students', 'heidimarasigan_nltc_class_records_students.class_record_id', '=', 'heidimarasigan_nltc_class_records.id')
            ->where('heidimarasigan_nltc_class_records_students.student_id',$student['id'])
            ->get();

        return $subjects_list;
    }

    public function getStudentID()
    {
        return Student::where('user_id',$this->user->id)->first()['student_code'];
    }
}