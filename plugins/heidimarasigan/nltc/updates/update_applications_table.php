<?php namespace HeidiMarasigan\Nltc\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateApplicationsTable extends Migration
{
    public function up()
    {
        Schema::table('heidimarasigan_nltc_applications', function(Blueprint $table) {
            $table->string('user_id')->nullable()->after('id');
            $table->text('child_name',255)->after('children');
            $table->text('child_age',255)->after('child_name');
            $table->text('child_gender',255)->after('child_age');
            $table->text('reference_name',255)->after('references');
            $table->text('reference_address',255)->after('reference_name');
            $table->text('reference_relationship',255)->after('reference_address');
            $table->text('reference_contactno',255)->after('reference_relationship'); 
            $table->text('christian_training_type',255)->after('christian_trainings');
            $table->text('christian_training_venue',255)->after('christian_training_type');
            $table->text('christian_training_date',255)->after('christian_training_venue');	
            $table->string('registration_code',20)->after('status');
        }); 
    }

    public function down()
    {
        // Schema::dropIfExists('heidimarasigan_nltc_applications');
    }
}
