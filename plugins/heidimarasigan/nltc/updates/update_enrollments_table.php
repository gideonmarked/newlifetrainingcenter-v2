<?php namespace HeidiMarasigan\Nltc\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateEnrollmentsTable extends Migration
{
    public function up()
    {
    	Schema::table('heidimarasigan_nltc_enrollments', function(Blueprint $table) {
            $table->integer('user_id')->after('id');
        });

        Schema::table('heidimarasigan_nltc_class_records_students', function(Blueprint $table) {
            $table->string('enrollment_id');
        }); 
    }

    public function down()
    {
    	Schema::dropIfExists('heidimarasigan_nltc_enrollments');
    }
}
