<?php namespace HeidiMarasigan\Nltc;

use System\Classes\PluginBase;
use App;
use Event;
use Backend;
use System;
use Backend\Models\User as BackendUserModel;
use Backend\Models\UserGroup as BackendUserGroupModel;
use Backend\Controllers\Users as BackendUsersController;
use HeidiMarasigan\Nltc\Controllers\Faculties;
use HeidiMarasigan\Nltc\Models\Faculty;
use HeidiMarasigan\Nltc\Controllers\Students;
use HeidiMarasigan\Nltc\Models\Student;




class Plugin extends PluginBase
{
	public function pluginDetails()
    {
        return [
            'name' => 'NLTC Bible School',
            'description' => 'New Life Training Center Bible School',
            'author' => 'Heidi Marasigan',
            'icon' => 'oc-icon-institution'
        ];
    }

    public function registerPermissions()
    {


        return [
                    'heidimarasigan.nltc.manage_applications' => [
                        'tab' => 'Manage Applications',
                        'label' => 'Manage NLTC Applicants',
                    ],
                    'heidimarasigan.nltc.new_applicant' => [
                        'tab' => 'Manage Applications',
                        'label' => 'NLTC Application Form',
                    ],
                    'heidimarasigan.nltc.manage_nltcportal' => [
                        'tab' => 'NLTC Portal',
                        'label' => 'Manage NLTC Portal',
                    ],
                    'heidimarasigan.nltc.manage_subjects' => [
                        'tab' => 'Manage Subjects',
                        'label' => 'Manage NLTC Subjects',
                    ],
                    'heidimarasigan.nltc.manage_students' => [
                        'tab' => 'Manage Students',
                        'label' => 'Manage NLTC Students',
                    ],
                    'heidimarasigan.nltc.manage_levels' => [
                        'tab' => 'Manage Levels',
                        'label' => 'Manage NLTC Levels',
                    ],
                    'heidimarasigan.nltc.manage_enrollments' => [
                        'tab' => 'Manage Enrollment',
                        'label' => 'Manage NLTC Enrollment',
                    ],
        ];
    }

    public function registerNavigation()
    {
        return [
            'NLTC' => [
                'label' => 'NLTC',
                'url' => 'heidimarasigan/nltc/applicants',
                'icon' => 'icon-institution',
                'permissions' => ['heidimarasigan.nltc.manage_nltcportal'],
                'sideMenu' => [
                    'applicants' => [
                            'label' => 'Applicants',
                            'url' => 'applicants',
                            'icon' => 'icon-group',
                            'permissions' => ['heidimarasigan.nltc.manage_applications']
                    ],
                    
                    'classrecords' => [
                        'label' => 'Class Records',
                        'url' => 'classrecords',
                        'icon' => 'icon-book',
                        'permissions' => ['heidimarasigan.nltc.manage_nltcportal']
                    ],
                    
                    'subjects' => [
                        'label' => 'Subjects',
                        'url' => 'subjects',
                        'icon' => 'icon-file-text-o',
                        'permissions' => ['heidimarasigan.nltc.manage_subjects'],
                    ],

                    'faculty' => [
                        'label' => 'Faculty',
                        'url' => 'faculties',
                        'icon' => 'icon-clipboard',
                        'permissions' => ['heidimarasigan.nltc.manage_nltcportal']
                    ],

                    'levels' => [
                        'label' => 'Levels',
                        'url' => 'levels',
                        'icon' => 'icon-sitemap',
                        'permissions' => ['heidimarasigan.nltc.manage_levels']
                    ],

                    'students' => [
                        'label' => 'Students',
                        'url' => 'students',
                        'icon' => 'icon-child',
                        'permissions' => ['heidimarasigan.nltc.manage_students']
                    ],

                    'enrollments' => [
                        'label' => 'Enrollments',
                        'url' => 'enrollments',
                        'icon' => 'icon-child',
                        'permissions' => ['heidimarasigan.nltc.manage_students']
                    ],

                ]
                                    
            ],

            'viewenrollment' => [
                'label' => 'My Subjects',
                'url' => 'http://localhost/newlifetrainingcenter-v2/dashboard/heidimarasigan/nltc/enrollments/preview',
                'icon' => 'icon-book',
                'permissions' => ['heidimarasigan.nltc.manage_enrollments']
            ],

            'createenrollment' => [
                'label' => 'My Enrollment',
                'url' => 'http://localhost/newlifetrainingcenter-v2/dashboard/heidimarasigan/nltc/enrollments/create',
                'icon' => 'icon-plus-square',
                'permissions' => ['heidimarasigan.nltc.manage_enrollments']
            ],

        ];
    }
    
    public function registerComponents()
    {
				return [
            'HeidiMarasigan\Nltc\Components\ApplicationForm'							=> 'applicationform',
            'HeidiMarasigan\Nltc\Components\AccountCreation'							=> 'accountcreation',
        ];
    }

    public function registerSettings()
    {
    }
		
	public function boot()
	{
		BackendUserModel::extend(function($model) {
            $model->hasOne['applications'] = ['HeidiMarasigan\Nltc\Models\Application'];
        });
	}

	public function registerFormWidgets()
	{
	    return [
	        'HeidiMarasigan\Nltc\FormWidgets\CustomDatePicker' => [
	            'label' => 'Custom Date Picker',
	            'code'  => 'customdatepicker'
	        ]
	    ];
	}

	public function registerMailTemplates()
    {
        return [
            'heidimarasigan.nltc::mail.admission' => 'Email when an application is approved.',
        ];
    }

}
