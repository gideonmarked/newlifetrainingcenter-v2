/*
 * This is a bundle file, you can compile this in two ways:
 * (1) Using your favorite JS combiner
 * (2) Using CLI command:
 *   php artisan october:util compile assets
 *
 * @see build-min.js
 *

=require jquery-1.10.2.js
=require jquery-ui.js

*/
