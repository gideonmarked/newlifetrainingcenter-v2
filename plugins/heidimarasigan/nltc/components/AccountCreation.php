<?php namespace HeidiMarasigan\Nltc\Components;

use Cms\Classes\ComponentBase;
use HeidiMarasigan\Nltc\Models\Applications;
use HeidiMarasigan\Nltc\Models\Profile;
use HeidiMarasigan\Nltc\Models\Level;
use HeidiMarasigan\Nltc\Models\Student;
use Backend\Models\User;
use Backend\Models\UserGroup;

use Input;
use Request;

class AccountCreation extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'AccountCreation Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun()
    {
        $applicant = Applications::where('registration_code', $this->param('code'))->first();
        if($applicant)
        {
            $this->page['email'] = $applicant->email;
            $student = Student::where('user_id',$applicant->user_id)->first();
            $this->page['student_code'] = $student->student_code;

        }

    }

    public function onCreate()
    {
        $application = Applications::where( 'email' , post('user_email') )->first();
        $user = User::find($application->user_id);
        $user->password = post('user_password');
        $user->password_confirmation = post('user_retype_password');
        $user->is_activated = 1;
        $user->save();


    }

    public function defineProperties()
    {
        return [];
    }

}