<?php namespace HeidiMarasigan\Nltc\Models;

use Model;
use HeidiMarasigan\Nltc\Models\Student;
use HeidiMarasigan\Nltc\Models\Subject;
use HeidiMarasigan\Nltc\Models\ClassRecord;
use Backend\Models\User;
use Log;
use Db;

/**
 * Enrollment Model
 */
class Enrollment extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'heidimarasigan_nltc_enrollments';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => 'Backend\Models\User',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function beforeSave()
    {
        $post = post();
        if($post['student_id']) {
            $student = Student::where('student_code', $post['student_id'] )->first();
            $user = User::find( $student['user_id'] );
            $this->user = $user;
            
        }
        
    }

    public function afterSave()
    {
        $student = Student::where('user_id', $this->user_id )->first();
        $subjects = Subject::where('level_id', $student['level_id'] )->get();
        $enrollment_subjects = array();
        foreach ($subjects as $subject) {
            $class_record = ClassRecord::where('subject_id',$subject['id'])->first();
            if( $class_record ) {
                Db::table('heidimarasigan_nltc_class_records_students')->insert(
                    [
                        'class_record_id' => $class_record['id'],
                        'student_id' => $student['id'],
                        'grade' => 60,
                        'evaluation' => 'pending',
                        'enrollment_id' => $this->id
                    ]
                );
            }
        }
    }

}